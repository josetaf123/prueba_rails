class SolutionController < ApplicationController

  before_action :set_numbers

  def index
  end

  def lower_number
    $pubnub.publish(
      channel: "optimiza_chile",
      message: { title: 'Un chiste', text: @numbers }
    )
    @result = @numbers.sort[0]

    render :result, layout: false
  end

  def geater_number
    $pubnub.publish(
      channel: "optimiza_chile",
      message: { title: 'Un chiste', text: @numbers }
    )
    @result = @numbers.sort[-1]

    render :result, layout: false
  end


  private

  def set_numbers
    @numbers = [2, 4, 5, 6, 7, 84, 3, 34, 6, 2, 3, 45]
  end

end

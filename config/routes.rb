Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'solution#index'

  get 'lower_number', to: 'solution#lower_number'
  get 'geater_number', to: 'solution#geater_number'

end
